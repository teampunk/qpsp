"""importtoexportdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import *
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from main.views import HomeView, CategoryView, NewsDetailView, VideosView
from django.conf.urls import *
from django.http import HttpResponse
from main import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.landing, name="landing"),
    url(r'^home/$', HomeView.as_view(), name="home"),
    url(r'^videos', VideosView.as_view(), name="videos_view"),
    url(r'^(?P<slug>[\w\-]+)/$', CategoryView.as_view(), name="category_view"),
    #url(r'^noticia/(?P<slug>[\w\-]+)/$', NewsDetailView.as_view(), name="news_detail"),
    #url(r'^(?P<catslug>[\w\-]+)/(?P<slug>[\w\-]+)/$', NewsDetailView.as_view(), name="news_detail"),
    url(r'^(?P<category>[\w\-]+)/(?P<slug>[\w\-]+)/$', NewsDetailView.as_view(), name="news_detail"),
    url(r'^suscripcion', views.contactUs, name="suscripcion"),
    url(r'^franquiciatario', views.franquiciatario, name="franquiciatario"),
    url(r'^search_news', views.search_news, name='search_news'),
    url(r'^resultado-de-busqueda', views.resultado_de_busqueda, name='resultado_de_busqueda'),
    url(r'^aviso-de-privacidad', views.terms_and_conditions, name='terms_and_conditions'),

    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns.append(url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
