from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from django.template.defaultfilters import slugify
from autoslug import AutoSlugField

# Create your models here.


class Test(models.Model):
    date = models.DateField()
    title = models.TextField()

class Author(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Book(models.Model):
    name = models.CharField('Book name', max_length=100)
    author = models.ForeignKey(Author, blank=True, null=True)
    author_email = models.EmailField('Author email', max_length=75, blank=True)
    imported = models.BooleanField(default=False)
    published = models.DateField('Published', blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    categories = models.ManyToManyField(Category, blank=True)

    def __unicode__(self):
        return self.name

class PromotionBanner(models.Model):
    titulo = models.CharField(max_length=100, blank=True)
    banner_url = models.CharField(max_length=1000, blank=True)
    banner_image = models.ImageField(upload_to='Images/PromotionBanners/')
    def __unicode__(self):
        return self.titulo

class BannerInSection(models.Model):
    titulo = models.CharField(max_length=100, blank=True)
    def __unicode__(self):
        return self.titulo

class BannerItem(models.Model):
    titulo = models.CharField(max_length=50,blank=True,null=True)
    descripcion = models.CharField(max_length=250, blank=True)
    slide_url = models.CharField(max_length=500, blank=True)
    image = models.ImageField(upload_to='Images/Benner/Items/')
    banner_in_section = models.ForeignKey(BannerInSection, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class SectionSlide(models.Model):
    nombre = models.CharField(max_length=100, blank=True)
    def __unicode__(self):
        return self.nombre

class SectionSlideItem(models.Model):
    titulo = models.CharField(max_length=50,blank=True,null=True)
    descripcion = models.CharField(max_length=250, blank=True)
    slide_url = models.CharField(max_length=500, blank=True)
    image = models.ImageField(upload_to='Images/Section/Slide/')
    section_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    def __unicode__(self):
        return self.titulo

class Tag(models.Model):
    name = models.CharField('Tags name', max_length=100)

    def __unicode__(self):
        return self.name

class Metatag(models.Model):
    titulo = models.CharField(max_length=100,blank=True)
    descripcion = models.CharField(max_length=1000,blank=True)
    keywords = models.CharField(max_length=1000,blank=True)
    def __unicode__(self):
        return self.titulo

class Video(models.Model):
    titulo = models.CharField(max_length=200, blank=True)
    descripcion = RichTextField(max_length=10000, blank=True, null=True)
    id_video = models.CharField(max_length=20, blank=True)

class Section(models.Model):
    titulo = models.CharField('Nombre de Seccion', max_length=100)
    descripcion = RichTextField(max_length=10000, blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    section_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    banner = models.ForeignKey(BannerInSection, blank=True, null=True)
    promotion_banner = models.ForeignKey(PromotionBanner, blank=True, null=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Section, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.titulo

class CategoryNew(models.Model):
    name = models.CharField('Nombre de Categoria', max_length=100)
    slug = models.SlugField(unique=True, blank=True, null=True)
    category_slide = models.ForeignKey(SectionSlide, blank=True, null=True)
    banner = models.ForeignKey(BannerInSection, blank=True, null=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(CategoryNew, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

class New(models.Model):
    titulo = models.CharField('News name', max_length=10000)
    categoria = models.ForeignKey(CategoryNew, related_name='children')
    imagen_thumbnail = models.ImageField(upload_to="images", blank=True, null=True)
    imagen_slide = models.ImageField(upload_to="images", blank=True, null=True)
    imagen1 = models.ImageField(upload_to="images", blank=True, null=True)
    sumario = models.TextField(max_length=10000, blank=True, null=True)
    texto1 = RichTextField(max_length=10000, blank=True, null=True)
    imagen2 = models.ImageField(upload_to="images", blank=True, null=True)
    texto2 = models.TextField(max_length=10000, blank=True, null=True)
    destacado1 = models.TextField(max_length=10000, blank=True, null=True)
    texto3 = models.TextField(max_length=10000, blank=True, null=True)
    imagen3 = models.ImageField(upload_to="images", blank=True, null=True)
    texto4 = models.TextField(max_length=10000, blank=True, null=True)
    destacado2 = models.TextField(max_length=10000, blank=True, null=True)
    texto5 = models.TextField(max_length=10000, blank=True, null=True)
    imagen4 = models.ImageField(upload_to="images", blank=True, null=True)
    texto6 = models.TextField(max_length=10000, blank=True, null=True)
    texto7 = models.TextField(max_length=10000, blank=True, null=True)
    imagen5 = models.ImageField(upload_to="images", blank=True, null=True)
    etiquetas = models.TextField(max_length=200, blank=True, null=True)
    date = models.DateField('Published', blank=True, null=True)
    slug = models.SlugField(unique=True, blank=True, null=True)
    #slug = AutoSlugField(populate_from=titulo, unique_with='categoria_slug',blank=True, null=True, editable=True)
    metatag = models.ForeignKey(Metatag, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(New, self).save(*args, **kwargs)

    def get_previous(self):
        """
        Returns the previous element of the list using the current index if it exists.
        Otherwise returns an empty string.
        """
        previous = self.__class__.objects.filter(pk__lt=self.pk).order_by('-pk')
        try:
            return previous[0]
        except IndexError:
            return False

    def get_next(self):
        """
        Get the next object by primary key order
        """
        next = self.__class__.objects.filter(pk__gt=self.pk)
        try:
            return next[0]
        except IndexError:
            return False

    def __unicode__(self):
        return self.titulo

class NewsLetterSuscription(models.Model):
    correo_electronico = models.TextField(max_length=100, blank=True, null=True)
    def __unicode__(self):
        return self.correo_electronico

class Contacto(models.Model):
    nombre_contacto = models.CharField(max_length=100)
    sexo = models.CharField(max_length=100)
    edad = models.CharField(max_length=10)
    ciudad = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)
    empresa = models.CharField(max_length=100)
    correo_electronico = models.CharField(max_length=150,blank=True,null=True)

    def __unicode__(self):
        return self.nombre_contacto

class Franquiciatario(models.Model):
    nombre_contacto = models.CharField(max_length=200,blank=True,null=True)
    correo_electronico = models.CharField(max_length=150,blank=True,null=True)
    telefono = models.CharField(max_length=100,blank=True,null=True)
    comentario = models.CharField(max_length=1500,blank=True,null=True)    

    def __unicode__(self):
        return self.nombre_contacto
