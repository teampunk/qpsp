from django import forms

class FormBuscarElementos(forms.Form):
	term = forms.CharField(max_length=100)

class Contacto(forms.Form):
    user_name = forms.CharField(max_length=100, required=True)
    user_sex = forms.CharField(required=True, max_length=13)
    user_age = forms.CharField(max_length=80)
    user_city = forms.CharField(max_length=80)
    user_state = forms.CharField(max_length=80)
    user_email = forms.EmailField(required=True)

class Franquiciatario(forms.Form):
    user_name = forms.CharField(max_length=100, required=True)
    user_email = forms.EmailField(required=True)
    user_phone_number = forms.CharField(required=True, max_length=13)
    user_message = forms.CharField(widget=forms.Textarea, max_length=250)