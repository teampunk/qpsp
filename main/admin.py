from django.contrib import admin
from .models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin


CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

# class BookResource(ImportExportModelAdmin):

#     class Meta:
#         model = Book


# class AuthorResource(ImportExportModelAdmin):

#     class Meta:
#         model = Author


# class TestResource(ImportExportModelAdmin):

#     class Meta:
#         model = Test


# class TagResource(ImportExportModelAdmin):

#     class Meta:
#         model = Tag


class SlideItemsInline(admin.TabularInline):
    model = SectionSlideItem

class SlideAdmin(admin.ModelAdmin):
    list_display = ['id','nombre']
    list_display_links = ['id','nombre']
    inlines = [SlideItemsInline]

class BannerItems(admin.TabularInline):
    model = BannerItem

class BannerAdmin(admin.ModelAdmin):
    list_display = ['id','titulo']
    list_display_links = ['id','titulo']
    inlines = [BannerItems]

class NewResource(ImportExportModelAdmin):
    list_display = ['id','titulo']
    list_display_links = ['id','titulo']
    class Meta:
        model = New

class MetatagAdmin(admin.ModelAdmin):
    list_display        = ['titulo']
    list_display_links  = ['titulo']

class VideoAdmin(admin.ModelAdmin):
    list_display        = ['id', 'titulo', 'id_video']
    list_display_links  = ['id', 'titulo', 'id_video']


# admin.site.register(Test, TestResource)
# admin.site.register(Category)
# admin.site.register(Author, AuthorResource)
# admin.site.register(Book, BookResource)
admin.site.register(Metatag, MetatagAdmin)
admin.site.register(SectionSlide, SlideAdmin)
admin.site.register(BannerInSection, BannerAdmin)
admin.site.register(PromotionBanner)
admin.site.register(New, NewResource)
admin.site.register(CategoryNew)
admin.site.register(Section)
admin.site.register(Video, VideoAdmin)
admin.site.register(Franquiciatario)
admin.site.register(Contacto)
admin.site.register(NewsLetterSuscription)
#admin.site.register(Tag, TagResource)
