# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-13 21:18
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Book name')),
                ('author_email', models.EmailField(blank=True, max_length=75, verbose_name='Author email')),
                ('imported', models.BooleanField(default=False)),
                ('published', models.DateField(blank=True, null=True, verbose_name='Published')),
                ('price', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='CategoryNew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Book name')),
            ],
        ),
        migrations.CreateModel(
            name='New',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=10000, verbose_name='News name')),
                ('imagen_thumbnail', models.ImageField(blank=True, null=True, upload_to='images')),
                ('imagen_slide', models.ImageField(blank=True, null=True, upload_to='images')),
                ('imagen1', models.ImageField(blank=True, null=True, upload_to='images')),
                ('sumario', models.TextField(max_length=10000)),
                ('texto1', ckeditor.fields.RichTextField(max_length=10000)),
                ('imagen2', models.ImageField(blank=True, null=True, upload_to='images')),
                ('texto2', models.TextField(blank=True, max_length=10000, null=True)),
                ('destacado1', models.TextField(max_length=10000)),
                ('texto3', models.TextField(max_length=10000)),
                ('imagen3', models.ImageField(blank=True, null=True, upload_to='images')),
                ('texto4', models.TextField(max_length=10000)),
                ('destacado2', models.TextField(max_length=10000)),
                ('texto5', models.TextField(max_length=10000)),
                ('imagen4', models.ImageField(blank=True, null=True, upload_to='images')),
                ('texto6', models.TextField(max_length=10000)),
                ('texto7', models.TextField(max_length=10000)),
                ('imagen5', models.ImageField(blank=True, null=True, upload_to='images')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Published')),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.CategoryNew')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Tags name')),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('title', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='new',
            name='etiquetas',
            field=models.ManyToManyField(to='main.Tag'),
        ),
        migrations.AddField(
            model_name='book',
            name='categories',
            field=models.ManyToManyField(blank=True, to='main.Category'),
        ),
    ]
