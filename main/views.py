# -*- coding: utf-8 -*-
import os
from django import template
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.template import Context
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from .models import *
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import timezone
import json
from json import dumps
from django.core import serializers  
from .forms import *

class HomeView(ListView):
    model = New
    template_name = 'home.html'
    paginate_by = 6
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['section'] = Section.objects.get(slug='home')
        p = Paginator(New.objects.all().order_by('-pk'), self.paginate_by)
        most_viewed = New.objects.annotate(Count('titulo')).order_by('-titulo')[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        popup = "True"
        
        context['news'] = p.page(context['page_obj'].number)
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['categorias'] = CategoryNew.objects.all()
        context['popup'] = popup

        return context

class CategoryView(ListView):
    model = New
    context_object_name = 'noticias'
    template_name = 'category.html'
    paginate_by = 6

    def get_queryset(self):
        return New.objects.filter(categoria__slug=self.kwargs['slug']).order_by('date')

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        context['slug'] = self.kwargs['slug']
        context['section'] = CategoryNew.objects.get(slug=self.kwargs['slug'])
        popup = "True"
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['popup'] = popup
        #context['now'] = timezone.now()
        return context

class NewsDetailView(DetailView):
    model = New
    context_object_name = 'new'
    # Sacar los parametros de base de datos o sesion.
    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        related_news = New.objects.all().exclude(slug=self.kwargs['slug'])[:3]
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        context['related_news'] = related_news
        popup = "True"
        context['popup'] = popup
        # print categories_selected
        return context

    def get_template_names(self):
        return 'news_detail.html'

class VideosView(ListView):
    model = Video
    context_object_name = 'videos'
    
    # Sacar los parametros de base de datos o sesion.
    def get_context_data(self, **kwargs):
        context = super(VideosView, self).get_context_data(**kwargs)
        last_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        context['section'] = Section.objects.get(slug='videos')
        context['categorias'] = CategoryNew.objects.all()
        most_viewed = New.objects.annotate(Count('titulo'))[:5]
        context['list_most_viewed'] = most_viewed
        context['latest_videos'] = last_videos
        popup = "True"
        context['popup'] = popup
        # print categories_selected
        return context

    def get_template_names(self):
        return 'videos.html'

def contactUs(request):
    categorias = CategoryNew.objects.all()
    slug = 'suscripcion'
    section = Section.objects.get(slug=slug)
    error = ''
    form = None
    # if request.method == 'POST':
    #     form = Contacto(request.POST)
    #     if form.is_valid():
    #         from_mail = 'info@quepadreserpadres.com'
    #         user_name = form.cleaned_data['user_name']
    #         user_sex = form.cleaned_data['user_sex']
    #         user_age = form.cleaned_data['user_age']
    #         user_city = form.cleaned_data['user_city']
    #         user_state = form.cleaned_data['user_state']
    #         user_email = form.cleaned_data['user_email']

    #         subject = 'Nueva solicitud de informacion - quepadreserpadres.con/suscripcion'
    #         email_body = subject + '\n'
    #         email_body += '\nNombre: ' + user_name
    #         email_body += '\nCorreo: ' + user_email
    #         email_body += '\nSexo: ' + user_sex
    #         email_body += '\nEdad: ' + user_age
    #         email_body += '\nCiudad: ' + user_city
    #         email_body += '\nEstado: ' + user_state

    #         recipients = ['sebas@punkmkt.com']
    #         #recipients = ['sebas@punkmkt.com']
    #         mail_data = EmailMessage(subject, email_body, from_mail, recipients)
    #         mail_data.encoding = "utf-8"
    #         mail_data.send()

    #         contacto = Contacto(nombre_contacto=user_name,sexo=user_sex,edad=user_age,ciudad=user_city,estado=user_state,empresa='',correo_electronico=user_email)
    #         contacto.save()
    #     else:
    #         error = 'faild'

    
    return render(request, 'contact-us.html', {'categorias':categorias, 'slug':slug, 'section':section, 'error':error, 'form':form})

def franquiciatario(request):
    categorias = CategoryNew.objects.all()
    section = Section.objects.get(slug='franquiciatario')

    error = ''
    form = None
    # if request.method == 'POST':
    #     form = Franquiciatario(request.POST)
    #     if form.is_valid():
    #         from_mail = 'info@quepadreserpadres.com'
    #         user_name = form.cleaned_data['user_name']
    #         user_email = form.cleaned_data['user_email']
    #         user_phone_number = form.cleaned_data['user_phone_number']
    #         user_message = form.cleaned_data['user_message']

    #         subject = 'Nueva solicitud de suscripcion de franquiciatario - quepadreserpadres.con/franquiciatario'
    #         email_body = subject + '\n'
    #         email_body += '\nNombre: ' + user_name
    #         email_body += '\nCorreo: ' + user_email
    #         email_body += '\nNúmero de Teléfono: ' user_phone_number
    #         email_body += '\nMensaje: ' + user_message

    #         recipients = ['sebas@punkmkt.com']
    #         #recipients = ['sebas@punkmkt.com']
    #         mail_data = EmailMessage(subject, email_body, from_mail, recipients)
    #         mail_data.encoding = "utf-8"
    #         mail_data.send()

    #         franquicitario = Franquiciatario(nombre_contacto=user_name,correo_electronico=user_email,telefono=user_phone_number,comentario=user_message)
    #         franquicitario.save()
    #     else:
    #         error = 'faild'

    return render(request, 'franquiciatario.html', {'categorias':categorias, 'section':section, 'error':error, 'form':form})

def terms_and_conditions(request):
    categorias = CategoryNew.objects.all()
    list_most_viewed = New.objects.annotate(Count('titulo'))[:5]
    latest_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
    popup = "True"
    return render(request, 'terminos-y-condiciones.html', {'categorias':categorias, 'list_most_viewed':list_most_viewed, 'latest_videos':latest_videos, 'popup':popup})

def search_news(request):
    if request.is_ajax():
        phrase = request.GET['phrase']
        news = New.objects.filter(titulo__icontains=phrase)
        results = []
        for new in news:
            new_json = {}
            new_json['name'] = new.titulo
            results.append(new_json)
        data = dumps(results)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)


def resultado_de_busqueda(request):
    form = None
    if request.method == 'POST':
        categorias = CategoryNew.objects.all()
        list_most_viewed = New.objects.annotate(Count('titulo'))[:5]
        latest_videos = Video.objects.annotate(Count('titulo')).order_by('-titulo')[:3]
        popup = "True"
        form = FormBuscarElementos(request.POST)
        results = []
        if form.is_valid():
            title = form.cleaned_data['term']
            news = New.objects.filter(titulo__icontains = title)
            for new in news:
                new_json = {}
                new_json['id'] = new.pk
                new_json['titulo'] = new.titulo
                new_json['texto1'] = new.texto1
                new_json['imagen_thumbnail'] = new.imagen_thumbnail
                new_json['slug'] = new.slug
                new_json['slug_category'] = new.categoria.slug
                results.append(new_json)
            paginator = Paginator(results, 6)
            return render(request, 'resultado-de-busqueda.html', {'results':results, 'categorias':categorias, 'list_most_viewed':list_most_viewed, 'latest_videos':latest_videos, 'popup':popup})
    else:
        results = 'error'
        return render(request, 'resultado-de-busqueda.html', {'results':results})


def landing(request):
    return render(request, 'landing.html', {})        